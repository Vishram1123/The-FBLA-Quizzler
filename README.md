# The FBLA Quizzler
A feature-rich quiz program for FBLA

## Its design:
The FBLA Quizzler is built using Java Swing, which is based on AWT. I chose Swing because it is a robust and functionally rich GUI for Java. The program utilizes several functions to initialize the UI, download the text file, handle button presses, set up the different variables used for initializing the questions, and more. It uses If-Else statements, and, wherever possible, it uses Switch cases to handle different iterations of the program. It is a very user-friendly program, and it is compatible with all operating systems running [Java SE Development Kit 15.0](https://www.oracle.com/java/technologies/javase-jdk15-downloads.html) or higher. 

## Running The FBLA Quizzler:
1. Install [Java SE Development Kit 15.0](https://www.oracle.com/java/technologies/javase-jdk15-downloads.html) or higher on your computer.
    - For installation instructions, look [here](https://docs.oracle.com/en/java/javase/15/install/overview-jdk-installation.html#GUID-8677A77F-231A-40F7-98B9-1FD0B48C346A). 
2. Download the program Jar file from the [Releases](https://github.com/Vishram1123/The-FBLA-Quizzler/releases/) page.
    - [To compile from source](https://github.com/Vishram1123/The-FBLA-Quizzler/tree/main/Compile%20From%20SRC), install Eclipse, download the source code, [import it into Eclipse](https://github.com/Vishram1123/The-FBLA-Quizzler/blob/main/Compile%20From%20SRC/Import%20From%20SRC.mov?raw=true) (File > Import > Projects from Folder or Archive > SourceCode.Zip > Finish), and [export as a jar file](https://github.com/Vishram1123/The-FBLA-Quizzler/blob/main/Compile%20From%20SRC/Export%20From%20SRC.mov) (File > Export > Java > Runnable JAR File > /Path/To/Your/Dir.jar > Finish).
3. Double click on the Jar file. 
    - If you have problems, check out the [Help page](https://github.com/Vishram1123/The-FBLA-Quizzler/blob/main/Help.md).
4. Test your knowledge.

## How it works:
<html><body><iframe src="https://raw.githubusercontent.com/Vishram1123/The-FBLA-Quizzler/main/slideshow.html"></iframe></body></html>

